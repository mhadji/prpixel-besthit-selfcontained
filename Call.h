#ifndef _CALL
#define _CALL

#include "PrPixelModuleHits.h"
#include "SOAContainer/include/SOAContainer.h"
#include "SOAContainer/include/PrintableNullSkin.h"

#define FACTOR 1000

// SOA version with SOAContainer utilisation
namespace SOA {
    namespace Fields {
        using namespace SOATypelist;
        // since we can have more than one member of the same type in our
        // SOA object, we have to do some typedef gymnastics so the compiler
        // can tell them apart
        typedef struct : public wrap_type<float> { } m_x;
        typedef struct : public wrap_type<float> { } m_y;
        typedef struct : public wrap_type<float> { } m_z;
        // typedef struct : public wrap_type<float> { } m_wxerr;
        // typedef struct : public wrap_type<float> { } m_wyerr;
        // The previous line could be written:
        //struct y : public wrap_type<float> {};
    }

    template <typename NAKEDPROXY>
    class SOAPointProxy : public PrintableNullSkin<NAKEDPROXY> {
    public:

        template <typename... ARGS>
        SOAPointProxy(ARGS&&... args)
            : PrintableNullSkin<NAKEDPROXY>(std::forward<ARGS>(args)...) { }

        float x() const noexcept
        { return this-> template get<Fields::m_x>(); }
        float y() const noexcept
        { return this-> template get<Fields::m_y>(); }
        float z() const noexcept
        { return this-> template get<Fields::m_z>(); }
        /*
          float wxerr() const noexcept
          { return this-> template get<Fields::m_wxerr>(); }
          float wyerr() const noexcept
          { return this-> template get<Fields::m_wyerr>(); }
          float wx() const noexcept
          { return wxerr() * wxerr(); }
          float wy() const noexcept
          { return wyerr() * wyerr(); }
        */
    };

    // define the SOA container type
    typedef SOAContainer<
        std::vector, // underlying type for each field
        SOAPointProxy, // skin to "dress" the tuple of fields with
        // one or more wrapped types which each tag a member/field
        Fields::m_x,
        Fields::m_y,
        Fields::m_z> PrPixelHits;
    // define the SOAPoint itself
    typedef typename PrPixelHits::proxy PrPixelHit;
}

struct Call {

    float module_z;
    float xTol;
    float maxScatter;

    PrPixelHit h1;
    PrPixelHit h2;

    PrPixelModuleHits modulehits;

    SOA::PrPixelHits container; // soa container of PrPixelHits of current modulehits
    Call() = default;

    Call(std::vector<std::string>& buffer, bool soacontainer = false) {
        unsigned int i = 0;
        module_z   = strtof(buffer[i++].c_str(), nullptr);
        xTol       = strtof(buffer[i++].c_str(), nullptr);
        maxScatter = strtof(buffer[i++].c_str(), nullptr);

        h1 = PrPixelHit(buffer, ++i);
        h2 = PrPixelHit(buffer, ++i);

        modulehits = PrPixelModuleHits(buffer, ++i);

        // modulehits.hits() to SOAContainer version
        if(soacontainer) {
            container.reserve(modulehits.hits().size());
            for(auto& h: modulehits.hits()) {
                container.emplace_back(h.x(),
                                       h.y(),
                                       h.z()
                    );
            }
        }
    }

};


#endif
