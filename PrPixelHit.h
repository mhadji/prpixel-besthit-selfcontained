#ifndef PRPIXELHIT_H
#define PRPIXELHIT_H 1

#include <vector>
#include <tuple>
#include <cmath>

#include <string>
#include <cstring>
#include <cstdlib>


/** @class PrPixelHit PrPixelHit.h
 *  This defines the VP hits to be used in the pattern recognition.
 *
 *  @author Olivier Callot
 *  @date   2012-01-05
 */

class PrPixelHit final {
public:
    PrPixelHit() = default;

    /// Standard constructor
    PrPixelHit(const float x, const float y, const float z,
               const float wxerr, const float wyerr) :
        m_x(x),
        m_y(y),
        m_z(z),
        m_wxerr(wxerr),
        m_wyerr(wyerr) {}

    PrPixelHit(const std::vector<std::string>& buffer,
               unsigned int& i) {
        m_x = strtof(buffer[i++].c_str(), nullptr);
        m_y = strtof(buffer[i++].c_str(), nullptr);
        m_z = strtof(buffer[i++].c_str(), nullptr);
        m_wxerr = strtof(buffer[i++].c_str(), nullptr);
        m_wyerr = strtof(buffer[i++].c_str(), nullptr);
    }

    friend std::ostream& operator<<(std::ostream& o,
                                    const PrPixelHit& p) {
        o << "hit\n"
          << p.m_x << "\n"
          << p.m_y << "\n"
          << p.m_z << "\n"
          << p.m_wxerr << "\n"
          << p.m_wyerr
          << std::endl;

        return o;
    }

    float x() const { return m_x; }
    float y() const { return m_y; }
    float z() const { return m_z; }
    float wx() const { return m_wxerr * m_wxerr; }
    float wy() const { return m_wyerr * m_wyerr; }
    float wxerr() const { return m_wxerr; }
    float wyerr() const { return m_wyerr; }
    bool isUsed() const { return m_isUsed; }
    void setUsed(const bool flag) { m_isUsed = flag; }

    /// Pointer to x,y,z,wx
    const float* p_x() const { return &m_x; }

    /// Calculate distance-square / sigma-square
    float chi2(const float x, const float y) const {
        const float dx = m_wxerr * (x - m_x);
        const float dy = m_wyerr * (y - m_y);
        return dx * dx + dy * dy;
    }

    // Operators for sorting the vectors of pointers to hits
    struct DecreasingByZ {
        bool operator()(const PrPixelHit* lhs, const PrPixelHit* rhs) const {
            return lhs->z() > rhs->z();
        }
    };
    struct IncreasingByZ {
        bool operator()(const PrPixelHit* lhs, const PrPixelHit* rhs) const {
            return lhs->z() < rhs->z();
        }
    };
    struct LowerByX {
        bool operator()(const PrPixelHit& lhs, const PrPixelHit& rhs) const {
            return lhs.x() < rhs.x();
        }
    };
    struct LowerByY {
        bool operator()(const PrPixelHit& lhs, const PrPixelHit& rhs) const {
            return lhs.y() < rhs.y();
        }
    };

private:
    /// Global x position
    float m_x;
    /// Global y position
    float m_y;
    /// Global z position
    float m_z;
    /// Weight (1 / error) in X
    float m_wxerr;
    /// Weight (1 / error) in Y
    float m_wyerr;
    /// Already used by (associated to) a track?
    bool m_isUsed = false;
};

typedef std::vector<PrPixelHit> PrPixelHits;

#endif  // PRPIXELHIT_H
