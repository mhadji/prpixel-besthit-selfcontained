#include <iostream>

#include <string>
#include <cstring>
#include <cstdlib>
#include <algorithm>
#include <iterator>
#include <fstream>
#include <chrono>
#include <time.h>

#include <valgrind/callgrind.h>

#ifdef INTELLINKED
#include "ittnotify.h"
#endif

#include "Call.h"

auto start = std::chrono::high_resolution_clock::now();
auto end = std::chrono::high_resolution_clock::now();
std::vector<Call> calls;

using vector_field = const std::vector<float, CacheLineAlignedAllocator<float>>;

//
// BESTHIT
//
float  __attribute__ ((noinline)) PrPT_bestHit(const SOA::PrPixelHits& module_hits,
                                               float module_z,
                                               const float xTol,
                                               const float maxScatter,
                                               const PrPixelHit& h1,
                                               const PrPixelHit& h2) {
    //if (modulehits.hits().empty()) return 0.;
    if (module_hits.size() == 0) return 0.;
    const float x1 = h1.x();
    const float x2 = h2.x();
    const float y1 = h1.y();
    const float y2 = h2.y();
    const float z1 = h1.z();
    const float z2 = h2.z();
    const float td = 1.0 / (z2 - z1);
    const float txn = (x2 - x1);
    const float tx = txn * td;
    const float tyn = (y2 - y1);
    const float ty = tyn * td;
    // Extrapolate to the z-position of the module
    const float xGuess = x1 + tx * (module_z - z1);

    // If the first hit is already below this limit we can stop here.
    if (module_hits[module_hits.size()-1].x() < xGuess - xTol) return 0.;
    //if (hh_x.back() < xGuess - xTol) return 0.;
    if (module_hits[0].x() > xGuess + xTol) return 0.;
    //if (hh_x[0] > xGuess + xTol) return 0.;
    // Do a binary search through the hits.
    unsigned int hit_start(0);
    //unsigned int step(modulehits.hits().size());
    unsigned int step(module_hits.size());
    const unsigned int module_nhits(step);

    while (2 < step) {  // quick skip of hits that are above the X-limit
        step /= 2;
        //if ((module_hits[hit_start + step]).x() < xGuess - xTol) hit_start += step;
        if ((module_hits[hit_start + step].x()) < xGuess - xTol) hit_start += step;
    }
    // Find the hit that matches best.
    unsigned int nFound = 0;
    float bestScatter = maxScatter;
    //const PrPixelHit *bestHit = nullptr;
    float bestHit = 0;
    for (unsigned int i = hit_start; i < module_nhits; ++i) {
        const auto& hit = module_hits[i];
        const float hit_z = hit.z();
        const float hit_x = hit.x();
        float hit_y = hit.y();
        //const float hit_z = hh_z[i];
        //const float hit_x = hh_x[i];
        //float hit_y = hh_y[i];

        const float dz = hit_z - z1;
        const float xPred = x1 + tx * dz;
        const float yPred = y1 + ty * dz;

        // If x-position is above prediction + tolerance, keep looking.
        if (hit_x + xTol < xPred) continue;
        // If x-position is below prediction - tolerance, stop the search.
        if (hit_x - xTol > xPred) break;
        const float dy = yPred - hit_y;
        // Skip hits outside the y-position tolerance.
        if (fabs(dy) > xTol) continue;
        const float scatterDenom = 1.0 / (hit_z - z2);
        const float dx = xPred - hit_x;
        const float scatterNum = (dx * dx) + (dy * dy);
        const float scatter = scatterNum * scatterDenom * scatterDenom;
        if (scatter < bestScatter) {
            //bestHit = &hit;
            bestHit = hit.x();//hh_x[i];
            bestScatter = scatter;
        }
        if (scatter < maxScatter)
            ++nFound;
    }

    return bestHit;
}

//
// PARSE
//
void parse(char* path, int ncall_limit) {
    std::ifstream infile(path);

    unsigned int i = 0;
    std::string line;
    std::vector<std::string> buffer;

    // get number of calls
    std::getline(infile, line);
    int nbcalls = atoi(line.c_str());

    if(ncall_limit > nbcalls) {
        std::cerr << "nb calls limit ("<< ncall_limit <<") greater than total calls (" << nbcalls << ")" << std::endl;
        exit(EXIT_FAILURE);
    }

    calls.resize(ncall_limit);

    std::cout << "parsing " << ncall_limit << " calls among " << nbcalls << " ..." << std::endl;

    start = std::chrono::high_resolution_clock::now();
    while(i < ncall_limit
          && std::getline(infile, line)) {
        if(line == "BEGIN CALL")
            while(std::getline(infile, line) && line != "END") {
                buffer.push_back(line);
            }

        calls[i] = Call(buffer, true);
        buffer.clear();
        i++;
        if(i >= ncall_limit)
            break;
    }
    end = std::chrono::high_resolution_clock::now();

    auto diff = ::end - ::start;
    std::cout << "[ time for parsing + soa transition: " << std::chrono::duration <double, std::milli> (diff).count() << " ms" << std::endl;
}

//
// PROCESS BESTHIT
//
double process(int ncall_limit) {
    // -- start profiling
    std::cout << "\nbestHit processing ..." << std::endl;
#ifdef INTELLINKED
    __itt_resume();
#endif
    CALLGRIND_START_INSTRUMENTATION;
    // --

    auto mindiff = end - start;
#pragma noinline
    for(int k = 0; k < FACTOR; k++) {
        start = std::chrono::high_resolution_clock::now();
        for(auto& c : calls) {
            asm volatile ("" : "+m"(c));
            auto dummy = PrPT_bestHit(c.container,
                                      c.module_z,
                                      c.xTol,
                                      c.maxScatter,
                                      c.h1,
                                      c.h2);
            asm volatile ("" : "+m"(c), "+X"(dummy));
        }
        end = std::chrono::high_resolution_clock::now();
        auto cdiff = end - start;

        if(k == 0)
            mindiff = cdiff;

        if(cdiff < mindiff)
            mindiff = cdiff;
    }

    // -- end profiling
#ifdef INTELLINKED
    __itt_pause();
#endif

    CALLGRIND_STOP_INSTRUMENTATION;
    CALLGRIND_DUMP_STATS;
    // --

    return std::chrono::duration <double, std::milli> (mindiff).count();
}

//
// MAIN
//
int main(int argc, char* argv[]) {
    if(argc < 2) {
        std::cerr << "Usage: "
                  << argv[0] << " [FILEPATH]" << std::endl;
        exit(EXIT_FAILURE);
    }

    std::cout << "\n----\n"
              << "[soa manuel real] Benchmark on PrPixelTracking::bestHit()\n" << std::endl;

    std::cout << "- compiler: "
#ifdef __INTEL_COMPILER
              << "icpc"
#else
              << "g++"
#endif
              << std::endl;

    int ncall_limit = 10000;
    if(argc == 3)
        ncall_limit = atoi(argv[2]);

    parse(argv[1], ncall_limit);
    double mintime = process(ncall_limit);

    std::cout << "[ time on bestHit (min among " << FACTOR << " repeats of " << calls.size() << "calls) : "
              << mintime << " ms" << std::endl;

    // -- export result on csv file
    std::string fn = "d/" + std::string(argv[0]).substr(2);
    std::cout << "export to: " << fn << std::endl;
    std::ofstream out;

    out.open(fn.c_str(), std::ios_base::app);
    out << ncall_limit << ";" << mintime << std::endl;
    out.close();

    std::cout << "--- end" << std::endl;
    return 0;
}
