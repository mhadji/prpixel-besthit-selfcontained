#!/bin/sh

valgrind --tool=callgrind --collect-jumps=yes --cache-sim=yes --dump-instr=yes --instr-atstart=no $*
mv callgrind.out.* callgrind/

bash -ci "sms \"done: $*\" " 2>&1 /dev/null
