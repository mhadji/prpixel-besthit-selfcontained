CXX	      = g++
CXXFLAGS     := --std=c++11 -g -O3
CHECK_D      := $(shell test -d d || mkdir -v d) # to export cvs executions time, need "d" directory
CVMFS_ACCESS := $(shell if [ -d /cvmfs/projects.cern.ch/. ]; then echo TRUE; fi)

ifeq ($(CVMFS_ACCESS), TRUE)
	LDFLAGS += -I/cvmfs/projects.cern.ch/intelsw/psxe/linux/x86_64/2017/vtune_amplifier_xe/include/ \
	      /cvmfs/projects.cern.ch/intelsw/psxe/linux/x86_64/2017/vtune_amplifier_xe/lib64/libittnotify.a

	CXXFLAGS += -DINTELLINKED

  ifeq ($(CXX), g++)
	  LDFLAGS += -ldl
  endif

endif

all:	b_aos \
	b_soa_aslice \
	b_soa_container

b_aos: main_aos.cpp
	$(CXX) $(CXXFLAGS) -o $@ $< $(LDFLAGS)

b_soa_aslice: main_soa_aslice.cpp
	$(CXX) $(CXXFLAGS) -o $@ $< $(LDFLAGS)

b_soa_container: main_soa_container.cpp
	$(CXX) $(CXXFLAGS) -o $@ $< $(LDFLAGS)

.PHONY: clean
clean:
	rm -f b_*
