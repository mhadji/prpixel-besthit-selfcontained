#ifndef PRPIXELMODULEHITS_H
#define PRPIXELMODULEHITS_H 1

// Local
#include "PrPixelHit.h"

/** @class PrPixelModule PrPixelModule.h
 *  Class to hold hits for one VP module
 *
 *  @author Sebastien Ponce
 */
namespace {
    template <std::size_t N>
    struct GetN {
        template <typename T>
        auto operator()(T&& t) const ->
            decltype(std::get<N>(std::forward<T>(t))) {
            return std::get<N>(std::forward<T>(t));
        }
    };
}

class PrPixelModuleHits final {
public:

    PrPixelModuleHits() = default;
    PrPixelModuleHits(const std::vector<std::string>& buffer,
                      unsigned int& i) {

        m_lastHitX  = strtof(buffer[i++].c_str(), nullptr);
        m_firstHitX = strtof(buffer[i++].c_str(), nullptr);

        // hits
        m_hits.resize(atoi(buffer[i++].c_str()));

        unsigned int k = 0;
        while(buffer[i++] == "hit")
            m_hits[k++] = PrPixelHit(buffer, i);

        // --
        // xFrac
        k = 0;
        m_xFractions.resize(atoi(buffer[i++].c_str()));

        while(buffer[i++] != "--") {
            m_xFractions[k++] = strtof(buffer[i].c_str(), nullptr);
        }

        m_yFractions.resize(buffer.size() - i + 1);
        k = 0;
        for( ; i < buffer.size(); i++)
            m_yFractions[k++] = strtof(buffer[i].c_str(), nullptr);
    }

    friend std::ostream& operator<<(std::ostream& o,
                                    const PrPixelModuleHits& p) {

        o << "module\n"
          << p.m_lastHitX << "\n"
          << p.m_firstHitX << std::endl;

        /*
          for(PrPixelHit h : p.m_hits)
          o << h;
        */

        o << "--\n";
        for(float fx : p.m_xFractions)
            o << fx << std::endl;

        o << "--\n";
        for(float fy : p.m_yFractions)
            o << fy << std::endl;

        return o;
    }


    bool empty() const { return m_empty; }
    float lastHitX() const { return m_lastHitX; }
    void setLastHitX(const float x) { m_lastHitX = x; }
    float firstHitX() const { return m_firstHitX; }
    void setFirstHitX(const float x) { m_firstHitX = x; }
    const PrPixelHits& hits() const { return m_hits; }
    PrPixelHits& hits() { return m_hits; }
    const std::vector<float>& xFractions() const { return m_xFractions; }
    const std::vector<float>& yFractions() const { return m_yFractions; }

private:
    float m_lastHitX = -1;
    float m_firstHitX;
    /// Vector of pointers to hits
    PrPixelHits m_hits;
    std::vector<float> m_xFractions;
    std::vector<float> m_yFractions;
    bool m_empty = true;
};
#endif  // PRPIXELMODULEHITS_H
