## Benchmark on PrPixelTracking::bestHit()
    [Rec/Pr/PrPixel/src/PrPixelTracking.h]

### sources

- `main_aos.cpp` : original version with array of structure version
- `main_soa_aslice.cpp` : soa version on all hits of all calls of all events
- `main_soa_container.cpp` : soa version with [SOAContainer](https://gitlab.cern.ch/LHCbOpt/SOAContainer.git) foreach call structure

### run

Usage:
    `./foo [path to the event file] [number of calls to bestHit (default:10000)]`

### event files

can downloaded here: [http://hadjiszs.fr/prpixel/prpixel_calls/](http://hadjiszs.fr/prpixel/prpixel_calls/)
need to `gunzip` each files.

pattern: `evt_{NBR-OF-EVENTS-EXPORTED}`

really no-intelligent and raw way to export calls to bestHit:
I've just exported on minibrunel each arguments foreach calls to bestHit on `N` events.

| event file | size after unzip | number of calls |
|:----------:|------------------|:---------------:|
| `evt_1`    | 53M              | 22 725          |
| `evt_5`    | 296M             | 120 508         |
| `evt_10`   | 1.4G             | 382 964         |
| `evt_100`  | 9.1G             | 2 960 746       |
