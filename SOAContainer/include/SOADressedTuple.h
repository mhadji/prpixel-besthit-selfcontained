/* @file SOADressedTuple.h
 *
 * @author Manuel Schiller <Manuel.Schiller@cern.ch>
 * @date 2015-05-09
 */

#ifndef SOADRESSEDTUPLE_H
#define SOADRESSEDTUPLE_H

#include <tuple>

/** @brief dress std::tuple with the get interface of SOAObjectProxy
 *
 * @author Manuel Schiller <Manuel.Schiller@cern.ch>
 * @date 2015-05-09
 *
 * @tparam TUPLE        an instantiation of std::tuple
 * @tparam CONTAINER    underlying SOAContainer
 */
template <typename TUPLE, typename CONTAINER>
class DressedTuple : public TUPLE
{
    public:
        /// convenience typedef
        typedef DressedTuple<TUPLE, CONTAINER> self_type;

    private:
        /// helper for construction from related tuples
        template <size_t... IDXS, typename... ARGS>
        constexpr DressedTuple(std::index_sequence<IDXS...>,
                const std::tuple<ARGS...>& args) : TUPLE(
                    std::get<IDXS>(args)...)
        { }
        /// helper for construction from related tuples
        template <size_t... IDXS, typename... ARGS>
        constexpr DressedTuple(std::index_sequence<IDXS...>,
                const std::tuple<const ARGS&...>& args) : TUPLE(
                    std::get<IDXS>(args)...)
        { }
        /// helper for construction from related tuples
        template <size_t... IDXS, typename... ARGS>
        constexpr DressedTuple(std::index_sequence<IDXS...>,
                std::tuple<ARGS...>&& args) : TUPLE(
                    std::get<IDXS>(args)...)
        { }

    public:
        /// construct from naked tuple
        template <typename... ARGS>
        constexpr DressedTuple(const std::tuple<ARGS...>& args) :
            DressedTuple(std::make_index_sequence<sizeof...(ARGS)>(),
                    args)
        { }
        /// construct from naked tuple of const references
        template <typename... ARGS>
        constexpr DressedTuple(const std::tuple<const ARGS&...>& args) :
            DressedTuple(std::make_index_sequence<sizeof...(ARGS)>(),
                    args)
        { }
        /// move-construct from naked tuple
        template <typename... ARGS>
        constexpr DressedTuple(std::tuple<ARGS...>&& args) :
            DressedTuple(std::make_index_sequence<sizeof...(ARGS)>(),
                    std::move(args))
        { }

        /// forward constructor calls to TUPLE's constructor(s)
        template <typename... ARGS,
                 typename std::enable_if<
                     std::is_constructible<TUPLE, ARGS...>::value,
                    int>::type = 0>
        constexpr DressedTuple(ARGS&&... args) :
            TUPLE(std::forward<ARGS>(args)...) { }

        /// forward (copy) assignment to the TUPLE implementation
        template <typename ARG, typename std::enable_if<
            std::is_assignable<TUPLE, const ARG&>::value
            >::type = 0>
        self_type& operator=(const ARG& other) noexcept(noexcept(
                    std::declval<TUPLE>().operator=(other)))
        { TUPLE::operator=(other); return *this; }

        /// forward (move) assignment to the TUPLE implementation
        template <typename ARG, typename std::enable_if<
            std::is_assignable<TUPLE, ARG&&>::value
            >::type = 0>
        self_type& operator=(ARG&& other) noexcept(noexcept(
                    std::declval<TUPLE>().operator=(std::move(other))))
        { TUPLE::operator=(std::move(other)); return *this; }

        /// provide the member function template get interface of proxies
        template<typename CONTAINER::size_type MEMBERNO>
        auto get() noexcept -> decltype(std::get<MEMBERNO>(
                    *static_cast<self_type*>(nullptr)))
        { return std::get<MEMBERNO>(*this); }

        /// provide the member function template get interface of proxies
        template<typename CONTAINER::size_type MEMBERNO>
        auto get() const noexcept -> decltype(std::get<MEMBERNO>(
                    *static_cast<const self_type*>(nullptr)))
        { return std::get<MEMBERNO>(*this); }

        /// provide the member function template get interface of proxies
        template<typename MEMBER, typename CONTAINER::size_type MEMBERNO =
            CONTAINER::template memberno<MEMBER>()>
        auto get() noexcept -> decltype(std::get<MEMBERNO>(
                    *static_cast<self_type*>(nullptr)))
        {
            static_assert(CONTAINER::template memberno<MEMBER>() ==
                    MEMBERNO, "Called with wrong template argument(s).");
            return std::get<MEMBERNO>(*this);
        }

        /// provide the member function template get interface of proxies
        template<typename MEMBER, typename CONTAINER::size_type MEMBERNO =
            CONTAINER::template memberno<MEMBER>()>
        auto get() const noexcept -> decltype(std::get<MEMBERNO>(
                    *static_cast<const self_type*>(nullptr)))
        {
            static_assert(CONTAINER::template memberno<MEMBER>() ==
                    MEMBERNO, "Called with wrong template argument(s).");
            return std::get<MEMBERNO>(*this);
        }
};

#endif // SOADRESSEDTUPLE_H

// vim: sw=4:tw=78:ft=cpp:et
